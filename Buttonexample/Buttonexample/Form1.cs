﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buttonexample
{
    
    public partial class Form1 : Form
    {
        List<string> list = new List<string>();
        List<KeyValuePair<int, string>> list2 = new List<KeyValuePair<int, string>>();
        object test = "";
        string collection = "";
        public int checker = 1;
 
        public Form1()
        {
            InitializeComponent();
        }
        public void listadd(int i)//this is where the querys stay instead of these generic lists these will have loops iteration over the database adding a new list instance every time there is something new in the database it will have the ID and the string valuse of the description its possible these may have to be different methods but cross that when i make the full one
        {
            if (i == 1)//city area
            {
                list2.Add(new KeyValuePair<int, string>(4, "kthx"));
                list2.Add(new KeyValuePair<int, string>(6, "stuff"));
                list2.Add(new KeyValuePair<int, string>(7, "more stuff"));
                list2.Add(new KeyValuePair<int, string>(2, "testingstuff"));
                list2.Add(new KeyValuePair<int, string>(1, "doingstuff"));
                list2.Add(new KeyValuePair<int, string>(4, "nope"));
            }
            else if (i == 2)//urbanarea
            {
                list2.Add(new KeyValuePair<int, string>(1, "data"));
                list2.Add(new KeyValuePair<int, string>(2, "comp"));
                list2.Add(new KeyValuePair<int, string>(3, "315"));
                list2.Add(new KeyValuePair<int, string>(4, "doing"));

            }
            else if(i == 3)//locality
            {
                list2.Add(new KeyValuePair<int, string>(1, "causual"));
                list2.Add(new KeyValuePair<int, string>(2, "test"));
                list2.Add(new KeyValuePair<int, string>(6, "more test"));
                list2.Add(new KeyValuePair<int, string>(3, "doing"));
                list2.Add(new KeyValuePair<int, string>(4, "kthx"));
                list2.Add(new KeyValuePair<int, string>(4, "alldone"));
                list2.Add(new KeyValuePair<int, string>(2, "last"));
                list2.Add(new KeyValuePair<int, string>(4, "one"));
            }
       
            return;
        }
        void MyButtonClick(object sender, EventArgs e)
        {
            test = (sender as Button).Tag;
            listadd(checker);
            List<Button> buttonsToDisable = new List<Button>()
            {
             button1,
             button2,
             button3,
             button4,
             button5,
             button6,
             button7,
             button8,
             button9
             };
            var i = 0;
            foreach (var itemA in buttonsToDisable)
            {
                if (i < list2.Count)
                {
                    itemA.Tag = list2[i].Key;
                    itemA.Text = list2[i++].Value;
                    itemA.Visible = true;

                }
                else
                {
                    if (itemA.Visible == true)
                    {
                        itemA.Visible = false;
                    }
                }
            }
            list2.Clear();
            checker++;
        }

        private void button10_Click(object sender, EventArgs e)//these two portions are just for testing purpose the infomation would be stored in 3 seprate variables and then when the submit button is pressed infomation is added to database and then cleared
        {
            collection += test.ToString();
 
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox1.Text = collection;
        }
    }
}
